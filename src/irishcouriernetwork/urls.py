"""irishcouriernetwork URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from rest_framework_jwt.views import obtain_jwt_token

urlpatterns = [
	#url(r'^', include('pages.urls', namespace='pages')), home page example
    url(r'^admin/', admin.site.urls),
    url(r'^account/', include('account.urls', namespace='account')),
    url(r'^account/config/', include('account.passwords.urls')),
    url(r'^', include('loads.urls', namespace='loads')),

    url(r'^api/auth/token/', obtain_jwt_token),
    url(r'^api/users/', include("account.api.urls", namespace='users-api')),
    url(r'^api/addresses/', include("addresses.api.urls", namespace='addresses-api')),
    url(r'^api/loads/', include('loads.api.urls', namespace='loads-api')),


]
