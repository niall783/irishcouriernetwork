import random
import string
import os

from django.utils.text import slugify


from django.conf import settings
import googlemaps
GOOGLE_MAPS_API_KEY = getattr(settings, 'GOOGLE_MAPS_API_KEY')



#=========================================================
#=====================Google Maps=========================
#=========================================================



def geocode(address):

    gmaps = googlemaps.Client(key=GOOGLE_MAPS_API_KEY)

    # Geocoding an address
    geocode_response = gmaps.geocode(address)
    
    if geocode_response:

        response = geocode_response[0]

        geometry = response['geometry']
        location = geometry['location']
        latitude = location['lat']
        longitude = location['lng']

        # print("Lat and Long")
        # print(latitude)
        # print(longitude)

    else:
        latitude = "false"
        longitude = "false"


    return latitude, longitude




    # # Look up an address with reverse geocoding
        # reverse_geocode_response = gmaps.reverse_geocode((54.256223, -8.476432))
        # response = reverse_geocode_response[0]
        # address_components = response['address_components']

        # field_zero = address_components[0]['long_name']
        # field_one = address_components[1]['long_name']
        # field_two = address_components[2]['long_name']
        # field_three = address_components[3]['long_name']
        # field_four = address_components[4]['long_name']
        # field_five = address_components[5]['long_name']
        # field_six = address_components[6]['long_name']



        # formatted_address = response['formatted_address']

        # print(address_components)
        # print(formatted_address)
        # print('---------------')
        # print(field_zero)
        # print(field_one)
        # print(field_two)
        # print(field_three)
        # print(field_four)
        # print(field_five)
        # print(field_six)














#=========================================================
#=====================Unique keys=========================
#=========================================================

def get_filename(path):
    return os.path.basename(path)


def random_string_generator(size=10, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))




def unique_key_generator(instance):
    """
    This is for a Django project with an key field.
    """
    size = random.randint(30,45)
    key = random_string_generator(size=size)

    Klass = instance.__class__
    qs_exists = Klass.objects.filter(key=key).exists()
    if qs_exists:
        return unique_key_generator(instance)
    return key






def unique_load_id_generator(instance):
    """
    This is for a Django project with an order id field.
    """
   
    load_new_id = random_string_generator().upper()

    Klass = instance.__class__
    qs_exists = Klass.objects.filter(load_id=load_new_id).exists()
    if qs_exists:
        return unique_load_id_generator(instance)
    return load_new_id






def unique_slug_generator(instance, new_slug=None):
    """
    This is for a Django project and it assumes your instance 
    has a model with a slug field and a title character (char) field.
    """
    if new_slug is not None:
        slug = new_slug
    else:
        slug = slugify(instance.title)

    Klass = instance.__class__
    qs_exists = Klass.objects.filter(slug=slug).exists()
    if qs_exists:
        new_slug = "{slug}-{randstr}".format(
                    slug=slug,
                    randstr=random_string_generator(size=4)
                )
        return unique_slug_generator(instance, new_slug=new_slug)
    return slug



