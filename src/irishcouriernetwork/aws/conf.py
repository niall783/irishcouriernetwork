import datetime 
import os

AWS_GROUP_NAME = "Engplaybook_Group"
AWS_USERNAME = "niall783"
AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID') 
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY')  





AWS_FILE_EXPIRE = 200
AWS_PRELOAD_METADATA = True

AWS_QUERYSTRING_AUTH = False

# AWS_DEFAULT_ACL = None


DEFAULT_FILE_STORAGE = 'engplaybook.aws.utils.MediaRootS3BotoStorage'
STATICFILES_STORAGE = 'engplaybook.aws.utils.StaticRootS3BotoStorage'
AWS_STORAGE_BUCKET_NAME = 'engplaybookbucket'
S3DIRECT_REGION = 'eu-west-2'
S3_URL = '//%s.s3.amazonaws.com/' % AWS_STORAGE_BUCKET_NAME
MEDIA_URL = '//%s.s3.amazonaws.com/media/' % AWS_STORAGE_BUCKET_NAME
MEDIA_ROOT = MEDIA_URL
STATIC_URL = S3_URL + 'static/'
ADMIN_MEDIA_PREFIX = STATIC_URL + 'admin/'

two_months = datetime.timedelta(days=61)
date_two_months_later = datetime.date.today() + two_months
expires = date_two_months_later.strftime("%A, %d %B %Y 20:00:00 GMT")

AWS_HEADERS = { 
    'Expires': expires,
    'Cache-Control': 'max-age=%d' % (int(two_months.total_seconds()), ),
}


PROTECTED_DIR_NAME = 'protected'
PROTECTED_MEDIA_URL = '//%s.s3.amazonaws.com/%s/' %( AWS_STORAGE_BUCKET_NAME, PROTECTED_DIR_NAME)

AWS_DOWNLOAD_EXPIRE = 5000 #(0ptional, in milliseconds



#The following warning was in the terminal - don't know what is means or what to do
#UserWarning: The default behavior of S3Boto3Storage is insecure and will change 
# in django-storages 2.0. By default files and new buckets are saved with an ACL 
#of 'public-read' (globally publicly readable). Version 2.0 will default to using 
#the bucket's ACL. To opt into the new behavior set AWS_DEFAULT_ACL = None, otherwise 
#to silence this warning explicitly set AWS_DEFAULT_ACL.


