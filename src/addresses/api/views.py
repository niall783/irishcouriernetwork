from rest_framework.views import APIView


from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
    )


from irishcouriernetwork.utils import geocode


from rest_framework.response import Response
from rest_framework.decorators import api_view
from rest_framework.decorators import parser_classes
from rest_framework.parsers import JSONParser



# @api_view(['GET', 'POST'])
# @parser_classes([JSONParser])
# def AddressGeoCodeAPIView(request, format=None):
#     """
#     A view that can accept POST requests with JSON content.
#     """
#     address = request.data['address']

#     lat, lng = geocode(address)

#     return Response({'lat': lat, 'lng':lng })


#This classed based view 
class AddressGeoCodeAPIView(APIView):
	parser_classes = [JSONParser]


	def post(self, request, format=None):
		address = request.data['address']
		lat, lng = geocode(address)

		return Response({'lat': lat, 'lng':lng })