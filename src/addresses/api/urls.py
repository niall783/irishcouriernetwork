
from django.conf.urls import url
from django.contrib import admin

from .views import (
    AddressGeoCodeAPIView,
    
    )




urlpatterns = [
    url(r'^geocode/$', AddressGeoCodeAPIView.as_view(), name='geocode-address'),
  
]