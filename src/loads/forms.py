from django import forms


from .models import Load


import datetime
from bootstrap_datepicker_plus import DatePickerInput






class LoadForm(forms.ModelForm):

	VEHICLE_CHOICES = (
		('Not Specified', 'Not Specified'),
		('Small Van','Small Van',),
		('Midi or SWB 2 -2.4m', 'Midi or SWB 2 -2.4m'),
		('MWB 3m', 'MWB 3m'),
		('LWB 3-4m', 'LWB 3-4m'),
		('XL WB 4m+', 'XL WB 4m+'),
		('Luton With Tail Lift', 'Luton With Tail Lift'),
		('Luton, No Lift', 'Luton, No Lift'),
		('Curtain Side', 'Curtain Side'),

	)



	
	pick_up_location		= forms.CharField(widget=forms.TextInput(attrs={"class":"autocomplete form-control","placeholder":"Pick Up Location" , 'id':"pick_up_location"}))
	drop_off_location		= forms.CharField(widget=forms.TextInput(attrs={"class":"autocomplete form-control","placeholder":"Drop Off Location" , 'id':"drop_off_location"}))
	
	

	#pick_up_time			= forms.DateTimeField(input_formats=['%Y-%m-%d %H:%M'])
	#input_formats=["%Y-%m-%d %H:%M:%S"]

	#input_formats=['%Y-%m-%d %H:%M'], 

	#widget=forms.DateInput(format = '%d/%m/%Y %H:%M')

	#start_date = forms.DateField(widget=forms.DateInput(format = '%d/%m/%Y'), input_formats=('%d/%m/%Y',))

	#widget=forms.DateInput(format = '%d/%m/%Y %H:%M'),

	#pick_up_time = forms.DateField( input_formats=('%d/%m/%Y %H:%M',))

	 # favorite_fruit= forms.CharField(label='What is your favorite fruit?', )


	pick_up_time 			= forms.DateTimeField(input_formats=('%d/%m/%Y %H:%M',), widget=forms.DateTimeInput(attrs={"class":"form-control","placeholder":"Pick Up Time", 'id': 'pick_up_time_picker'}))
	
	drop_off_time 			= forms.DateTimeField(input_formats=('%d/%m/%Y %H:%M',), widget=forms.DateTimeInput(attrs={"class":"form-control","placeholder":"Drop Off Time" , 'id': 'drop_off_time_picker'}))

	
	weight					= forms.DecimalField(widget=forms.TextInput(attrs={"class":"form-control","placeholder":"kg"}))

	vehicle_type			= forms.CharField(widget=forms.Select(choices=VEHICLE_CHOICES, attrs={"class":"form-control","placeholder":""}))


	notes					= forms.CharField(required=False, widget=forms.Textarea(attrs={"class":"form-control", 'rows':3, "placeholder":"Optional"}))


	#drop_off_time 			= forms.DateField(widget=forms.SelectTimeWidget(attrs={"class":"form-control","placeholder":"Drop Off Location"}))
	
	#drop_off_time			= forms.DateTimeField(widget=forms.SelectDateWidget(attrs={"class":"form-control","placeholder":"Drop Off Location", 'id':"drop_off_time"}))

	#pick_up_time 			= forms.DateTimeField(widget=forms.DateTimeInput(attrs={"class":"form-control","placeholder":"Pick up time"}))
	#drop_off_time 			= forms.DateField(widget=DatePickerInput(options={"format": "mm/dd/yyyy","autoclose": True}))


	#vehicle_type			= forms.ChoiceField()
	#short_description		= forms.CharField(widget=forms.TextInput(attrs={"class":"form-control","placeholder":"Short Description"}))
	#long_description		= forms.CharField(widget=forms.TextInput(attrs={"class":"form-control","placeholder":"Long Description"}))


#widget=forms.TextInput(attrs={"class":"form-control","placeholder":"Vehicle Type"})

#widget=forms.SelectDateWidget(attrs={"class":"form-control","placeholder":"Drop Off Location"})
	






	class Meta:
		model = Load
		
		fields = [
			"pick_up_location",
			"drop_off_location",
			"pick_up_time",
			"drop_off_time",
			"vehicle_type",
			"weight",
			"notes",
			
			
		]
		widgets = {
			#'pick_up_time': forms.DateTimeInput(attrs={"class":"form-control", 'id': 'pick_up_time_picker'}),
			#'drop_off_time': forms.DateTimeInput(attrs={"class":"form-control", 'id': 'drop_off_time_picker'}),

			}