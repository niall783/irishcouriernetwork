from rest_framework.generics import (
					CreateAPIView,
					ListAPIView,
					RetrieveAPIView,
					UpdateAPIView,
					RetrieveDestroyAPIView,	
					RetrieveUpdateAPIView)


from rest_framework.permissions import (
    AllowAny,
    IsAuthenticated,
    IsAdminUser,
    IsAuthenticatedOrReadOnly,
    )



from loads.models import Load

from .serializers import (
					LoadSerializer,
					LoadCreateUpdateSerializer,
					)


from .permissions import IsOwnerOrReadOnly

from geopy.distance import geodesic

import json

from geopy.distance import geodesic







class LoadCreateAPIView(CreateAPIView):
	queryset = Load.objects.all()
	serializer_class = LoadCreateUpdateSerializer
	# permission_classes = [AllowAny] #only for testing
	#permission_classes = [IsAuthenticated]

	def perform_create(self, serializer):
		serializer.save(user=self.request.user)




class LoadListAPIView(ListAPIView):
	queryset = Load.objects.all()
	serializer_class = LoadSerializer
	permission_classes = [AllowAny] #only for testing


	def get_queryset(self, *args, **kwargs):
	        #queryset_list = super(PostListAPIView, self).get_queryset(*args, **kwargs)

			# queryset_list = Load.objects.all() #filter(user=self.request.user)
			query = self.request.GET.get("q")

			# example = '{ "dist1": 100, "lat1": 54.259712, "long1": -8.473024 }'

			# example = '{ "dist1": 100, "lat1": 55.2053, "long1":  -6.2533 , "dist2": 100, "lat2": 54.7529, "long2": -6.0015 }'

			# example = '{ "dist1": 100, "lat1": 54.259712, "long1": -8.473024 , "dist2": null , "lat2": null , "long2": null}'

			#copy and paste into url bar
			#http://127.0.0.1:8000/api/loads/?q=%27{%22dist1%22:100,%22lat1%22:54.259712,%22long1%22:-8.473024}%27


			if query:
				

				json_response = json.loads(query.replace("'",""))

				#print("Json_response " + str(json_response))

				#user_latlng = (json_response['userlat'], json_response['userlong'])

				dist1 = json_response["dist1"]
				latlng1 = (json_response['lat1'], json_response['long1'])
				
				dist2 = json_response["dist2"]
				latlng2 	= (json_response['lat2'], json_response['long2'])

				pick_up_date = (json_response['pick_up_date']) #could do an option for the next week perhaps

				print(pick_up_date)

				queryset_list = Load.objects.dist_within_for_two_points( dist1, latlng1, dist2, latlng2).filter(pick_up_time__contains=pick_up_date)
				



				#converting the queryset to a list
				load_list = list(queryset_list)

				#looping through the list and adding in the distance from the user
				for load in load_list:

					load_pickup_location = (load.pick_up_location_lat, load.pick_up_location_lng)
					load_dropoff_location = (load.drop_off_location_lat, load.drop_off_location_lng)

					dist_to_pickup = geodesic(latlng1, load_pickup_location)
					dist_to_pickup = float(str(dist_to_pickup).replace("km","").replace(" ",""))
					load.dist_from_pickup_to_loc1 = dist_to_pickup

					#in this case the use has specified a desired dropoff location
					if latlng2[0] and latlng2[1]:
						dist_from_dropoff_to_loc2 = geodesic(latlng2, load_dropoff_location)
						dist_from_dropoff_to_loc2 = float(str(dist_from_dropoff_to_loc2).replace("km","").replace(" ",""))
						load.dist_from_dropoff_to_loc2 = dist_from_dropoff_to_loc2
						load.total_distance = dist_from_dropoff_to_loc2 + dist_to_pickup

						#print("dist_from_dropoff_to_loc2 " + str(dist_from_dropoff_to_loc2))
						#print("total_distance " + str(load.total_distance))


				#if latlng2[0] and latlng2[1] are not null then the loads will be orders as per the value "total_distance".
				#The value "total_distance" is an attempt to find and average distance taking into account the distance from the pickup location to their specified pickup location 
				#and the distance from the drop off to their specified drop off. 



				if latlng2[0] and latlng2[1]:
					#sorting the list in order by suming the distacne form the users location to the pick up and the distance from the drop off to a user specified location 
					load_list.sort(key=lambda x: x.total_distance, reverse=False)
				else:
					#sorting the list in order of distance from the user
					load_list.sort(key=lambda x: x.dist_from_pickup_to_loc1, reverse=False)


				return load_list #queryset_list

			else:
				return Load.objects.all()







class LoadDetailAPIView(RetrieveAPIView):
	queryset = Load.objects.all()
	serializer_class = LoadSerializer
	lookup_field = 'load_id'


class LoadDeleteAPIView(RetrieveDestroyAPIView):
	queryset = Load.objects.all()
	serializer_class = LoadSerializer
	lookup_field = 'load_id'


class LoadUpdateAPIView(RetrieveUpdateAPIView):
	queryset = Load.objects.all()
	serializer_class = LoadCreateUpdateSerializer
	# permission_classes = [IsOwnerOrReadOnly] #this is a custom permission
	permission_classes = [AllowAny]
	lookup_field = 'load_id'	