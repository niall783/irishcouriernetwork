from django.conf.urls import url

from .views import (
					LoadCreateAPIView,
					LoadListAPIView,
					LoadDetailAPIView,
					LoadDeleteAPIView,
					LoadUpdateAPIView)


# app_name = 'loads'
urlpatterns = [
		url(r'^create/$', LoadCreateAPIView.as_view(), name="load_create_api_view"),
		url(r'^$', LoadListAPIView.as_view(), name="load_list_api_view"),
		# url(r'^(?P<pk>\d+)/$', LoadDetailAPIView.as_view(), name="load_detail_api_view"),
		url(r'^(?P<load_id>[\w-]+)/$', LoadDetailAPIView.as_view(), name="load_detail_api_view"),
		url(r'^(?P<load_id>[\w-]+)/edit/$', LoadUpdateAPIView.as_view(), name="load_update_api_view"),
		url(r'^(?P<load_id>[\w-]+)/delete/$', LoadDeleteAPIView.as_view(), name="load_delete_api_view")	
		
]

# (?P<load_id>[\w-]+)