from rest_framework.serializers import (
							ModelSerializer, 
							HyperlinkedIdentityField, 
							SerializerMethodField)

from loads.models import Load 









class LoadCreateUpdateSerializer(ModelSerializer):
	class Meta:
		model = Load
		fields = [
			# 'id',
			# 'load_id',

			'client_name',				
			'client_phone_number ',	
			'payment_terms', 	

			'pick_up_location',
			'pick_up_location_lat', 	
			'pick_up_location_lng', 

			'drop_off_location',
			'drop_off_location_lat', 	
			'drop_off_location_lng',

			'pick_up_time',
			'drop_off_time',

			'vehicle_type',
			'weight',
			'notes',

			# 'dist_from_user',
			
			'updated',     			
			'timestamp',	
		
		]

	  		



load_detail_url = HyperlinkedIdentityField(
        view_name='loads-api:load_detail_api_view',
        lookup_field='load_id'
        )



class LoadSerializer(ModelSerializer):
	url = load_detail_url
	user = SerializerMethodField()

	

	class Meta:
		model = Load
		fields = [
			'id',
			'url',
			'load_id',
			'user',

			'client_name',				
			'client_phone_number',	
			'payment_terms', 	

			'pick_up_location',
			'pick_up_location_lat', 	
			'pick_up_location_lng', 

			'drop_off_location',
			'drop_off_location_lat', 	
			'drop_off_location_lng',

			'pick_up_time',
			'drop_off_time',

			'vehicle_type',
			'weight',
			'notes',
			
			#'dist_from_user',
			'dist_from_pickup_to_loc1',
			'dist_from_dropoff_to_loc2',
			'total_distance',

			'updated',     			
			'timestamp',
		]


		#this did not work and can be deleted
		# def to_representation(self, instance):
  #        # instance is the model object. create the custom json format by accessing instance attributes normaly and return it
		# 	data = super(LoadSerializer, self).all(*args, **kwargs)

		# 	print("Inside to_representation")

		# 	identifiers = dict()
		# 	identifiers['dist_from_user'] = 42.2
        

		# 	representation = {
		# 		'identifiers': identifiers,
		# 		'dist_from_user': 42.2,
		# 		# 'timestamp': instance.xxxxx,

			 
		# 	} 

		# 	return representation




	def get_user(self, obj):
		return str(obj.user)




