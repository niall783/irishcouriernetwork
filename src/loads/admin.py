from django.contrib import admin

# Register your models here.



from .models import Load

class LoadAdmin(admin.ModelAdmin):
    readonly_fields = ('timestamp','updated',)


    fieldsets = (
        (None, {'fields': ('load_id',)}),
        ('Client Info', {'fields': ('user','client_name','client_phone_number', 'payment_terms')}),
        
        ('Pick Up Info', {'fields': ('pick_up_location',
        							'pick_up_location_lat', 
        							'pick_up_location_lng',
        							'pick_up_time')}),   


        ('Drop Off Info', {'fields': ('drop_off_location', 
        							  'drop_off_location_lat', 
        							  'drop_off_location_lng',
        							  'drop_off_time',)}),

		('Misc', {'fields': ('vehicle_type','weight', 'notes', 'dist_from_pickup_to_loc1')}),

       
        
        ('Times', {'fields': ('timestamp', 'updated',)})
    )







admin.site.register(Load, LoadAdmin)