from django.shortcuts import render, redirect
from django.conf import settings

from django.views.generic import ListView, DetailView, View
from django.shortcuts import render, get_object_or_404, redirect
from django.http import Http404, HttpResponse, HttpResponseRedirect

# Create your views here.

from .forms import LoadForm

from .models import Load

# from geopy.distance import geodesic

import googlemaps
from datetime import datetime
 
import requests, json 

# from geopy.geocoders import Nominatim

# from mapbox import Geocoder

GOOGLE_MAPS_API_KEY = getattr(settings, 'GOOGLE_MAPS_API_KEY')

MAP_BOX_API_KEY = getattr(settings, 'MAP_BOX_API_KEY')




def google_places_autocomplete(query):

	url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?"

	# get method of requests module 
	# return response object 

	r = requests.get(url + 'input=' + query +
	                '&key=' + GOOGLE_MAPS_API_KEY)                         

	# json method of response object convert 
	#  json format data into python format data 

	response = r.json() 
	predictions = response['predictions']
	no_of_predictions = len(predictions)  #number_of_predictions

	descriptions = []

	for prediction in predictions:
		descriptions.append(prediction['description'])

	return descriptions, no_of_predictions



def google_places_text_search(query):

	url = "https://maps.googleapis.com/maps/api/place/textsearch/json?"

	# get method of requests module 
	# return response object 

	r = requests.get(url + 'query=' + query +
	                      '&key=' + GOOGLE_MAPS_API_KEY)      

	# json method of response object convert 
	#  json format data into python format data 

	response = r.json() 
	results = response['results']
	no_of_results = len(results)  #number of results


	formatted_addresses = []


	for result in results:
		formatted_addresses.append(result['formatted_address'])


	return formatted_addresses, no_of_results




	



def post_load(request):

	form = LoadForm(request.POST or None)

	address_list = []



	# if request.user.is_authenticated():

	if form.is_valid():

		if not request.user.is_authenticated():
			return redirect("account:login")
			
		instance = form.save(commit=False)
		instance.user = request.user
		instance.save()	
		
		return redirect("loads:post_load")

	# else:
	# 	return redirect("account:login")



		 




	if request.method == "POST":



		# newport_ri = (54.257947, -8.472049)
		# cleveland_oh = (54.252548, -5.949803)
		# print("Distance")
		# print(geodesic(newport_ri, cleveland_oh).miles)


		gmaps = googlemaps.Client(key=GOOGLE_MAPS_API_KEY)


		#address_list, number = google_places_autocomplete("5 mountain view")
		#print(address_list)
		#places, number =  google_places_text_search("142 rusheen ard")	
	

	
		#  {% include 'loads/snippets/google_map.html' with object=object apikey=apikey %} 
	# {% include 'loads/snippets/map_box.html' with object=object map_box_api_key=map_box_api_key %}

	


	context = {
		"form":form,
		"api_key":GOOGLE_MAPS_API_KEY,
		"address_list":address_list
	}

	

	return render(request, "loads/post_load_form.html", context)




def load_list_view(request, *args, **kwargs):
	
	queryset = Load.objects.all().order_by('-updated')

	# print(queryset)

	

	context = {
		"object_list":queryset
	}	

	
	
	return render(request,"loads/loads_list_view.html",context)
	








class LoadDetailView(DetailView):
	queryset = Load.objects.all()
	template_name = "loads/load_detail_view.html"

 	#{% include 'loads/snippets/map_box.html' with object=object map_box_api_key=map_box_api_key %}
	#{% include 'loads/snippets/google_map.html' with object=object apikey=apikey %}

	

	def get_context_data(self, *args, **kwargs):
		context = super(LoadDetailView,self).get_context_data(*args, **kwargs)
		instance = context['object'] #getting the load object i.e. the instance of the load
		#just in case i need to add additional context
		context['api_key'] = GOOGLE_MAPS_API_KEY
		context['map_box_api_key'] = MAP_BOX_API_KEY
		return context

		


	def get_object(self, *args, **kwargs):
		request = self.request
		#user = request.user
		load_id = self.kwargs.get('load_id')

		#instance = get_object_or_404(Tutorial, slug=slug)
		#return instance



		try:
			instance = Load.objects.get(load_id=load_id)
			print("Load id")
			print (instance)
		except Load.DoesNotExist:
			raise Http404("Not found..")
		except Load.MultipleObjectsReturned:
			qs = Load.objects.filter(load_id=load_id)
			instance = qs.first()
		except:
			raise Http404("Uhhmmm ")
		return instance	





#======================
#for imformation only =
#for imformation only =
#for imformation only =
#for imformation only =
#======================
# geolocator = Nominatim(user_agent="testing_app")
		# # location = geolocator.geocode("5 mountain castlewellan county down northern ireland")
		
		# location = geolocator.reverse("54.259505, -8.474914")
		# print(location.address)


		# print("Coordinates")
		# print(location)
		# print(location.address)
		# print((location.latitude, location.longitude))


		# geocoder = Geocoder(access_token="pk.eyJ1IjoibmlhbGw3ODMiLCJhIjoiY2p4OHQzNTNlMG1zZzN0bGdjbzNtMGl0dSJ9.xK47LUSgqsOsk1SV-ChWaA")
		
		# response = geocoder.forward("Newcastle Rd, Galway, H91 YR71")

		# features = response.geojson()['features'][0]
		# geometry = features['geometry']
		# coordinates = geometry['coordinates']

		# latitude = coordinates[0]
		# longitude  = coordinates[1]


		# print("JSON Response")
		# print("features " + str(features))
		# print("Coordinates: " + str(coordinates))
		# print("Lat: " + str(latitude))	
		# print("Long: " + str(longitude))
		# print(str(longitude) + " " + str(latitude))
