from django.conf.urls import url

from .views import (post_load,
					LoadDetailView,
					load_list_view)

# url(r'^$',views.IndexView.as_view(), name='index'),

#url(r'^login/$', login_page, name="login"),
#url(r'^register/$', register_page, name="register"),
#url(r'^login/$', LoginView.as_view(), name="login"),
#url(r'^register/$', RegisterView.as_view(), name="register"),


app_name = 'loads'
urlpatterns = [
		#url(r'^$',home_page, name='home'),
		#url(r'^login/$', login_page, name="login"),
		#url(r'^login/$', LoginView.as_view(), name="login"),
		url(r'^$', post_load, name="post_load"), # current home page
		url(r'^load/load_list_view/$', load_list_view, name="load_list_view"),
		url(r'^load/(?P<load_id>[\w-]+)/$', LoadDetailView.as_view(), name="load_detail_view"),
	

#(?P<load_id>\d+)
# (?P<load_id>[\w-]+)
	
		
]