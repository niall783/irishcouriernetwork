from django.db import models

# Create your models here.

from irishcouriernetwork.utils import unique_load_id_generator, unique_slug_generator, geocode
from django.db.models.signals import pre_save
from django.urls import reverse

from geopy.distance import geodesic

from django.conf import settings

from datetime import datetime

User = settings.AUTH_USER_MODEL


VEHICLE_CHOICES = (
		('Small Van','Small Van',),
		('Midi or SWB 2 -2.4m', 'Midi or SWB 2 -2.4m'),
		('MWB 3m', 'MWB 3m'),
		('LWB 3-4m', 'LWB 3-4m'),
		('XL WB 4m+', 'XL WB 4m+'),
		('Luton With Tail Lift', 'Luton With Tail Lift'),
		('Luton, No Lift', 'Luton, No Lift'),
		('Curtain Side', 'Curtain Side'),

	)






class LoadManager(models.Manager):

	# this function return loads were the pickup location is within a user defined radius (m) from the users current location 
	# function to return objects that are within a specific distance from the user
	# def distance_within(self, user_dist, user_latlng): 
	# 	qs = Load.objects.all()

	# 	wanted_ids = []

	# 	for load in qs:
	# 		load_location = (load.pick_up_location_lat, load.pick_up_location_lng)
	# 		distance = geodesic(user_latlng, load_location)
	# 		if distance <= user_dist:
	# 			wanted_ids.append(load.id)

	# 	return qs.filter(id__in=wanted_ids)



	# this function return loads were the pickup location is within a user defined radius (m) from the users current location 
	# and that the dropoff location is within a user defined radius of a user defined second location (e.g. their home)
	def dist_within_for_two_points(self, user_loc_search_radius, loc_one_latlng, loc_two_search_radius ,loc_two_latlng): 
		qs = Load.objects.all()

		wanted_ids = []


		dist1 = user_loc_search_radius
		latlng1 = loc_one_latlng

		dist2 = loc_two_search_radius
		latlng2 = loc_two_latlng

		# print("dist1 " + str(dist1))
		# print("latlng1 " + str(latlng1[0]))

		# print("dist2 " + str(dist2))
		# print("latlng2 " + str(latlng2))



		for load in qs:
			load_pickup_location = (load.pick_up_location_lat, load.pick_up_location_lng)
			load_dropoff_location = (load.drop_off_location_lat, load.drop_off_location_lng)


			dist_to_pickup = geodesic(latlng1, load_pickup_location)
			#print("dist_to_pickup " + str(dist_to_pickup))
			
			dist_from_dropoff_to_loc2 = geodesic(latlng2, load_dropoff_location)
			# print("dist_from_dropoff_to_loc2 " + str(dist_from_dropoff_to_loc2))



			# This first if statement is searching within a specified radius of their current loaction anc the load pickup location 
			# and searching within a specified radius of their second location (e.g. their home) and the drop off location 
			if dist1 and latlng1[0] and latlng1[1] and dist2 and latlng2[0] and latlng2[1]:
				if dist_to_pickup <= dist1 and dist_from_dropoff_to_loc2 <= dist2:
					wanted_ids.append(load.id)
					#load.dist_from_user = dist_to_pickup
					#load.save()
					#print("Scenario 1")

			# this elif statement is searching within a specified radis of the user's current location and the pick up location
			elif dist1 and latlng1[0] and latlng1[1]  and not dist2: #and not latlng2[0] and not latlng2[1]:
				if dist_to_pickup <= dist1:
					wanted_ids.append(load.id)
					load.dist_from_pickup_to_loc1 = dist_to_pickup
					

			# This elif statement is searching within a specified radius of their second location (e.g. their home) and the drop off location 
			#elif not dist1 and not latlng1[0] and not latlng1[0] and dist2 and latlng2[0] and latlng2[1]:
			elif not dist1  and dist2 and latlng2[0] and latlng2[1]:	
				if dist_from_dropoff_to_loc2 <= dist2:
					wanted_ids.append(load.id)
					
					

			# this elif statement is searching  for all loads
			elif not dist1 and latlng1[0] and latlng1[1]  and not dist2 and not latlng2[0] and not latlng2[1]:
				wanted_ids.append(load.id)
				

			# this elif statement is searching  for all loads when the user has specified a particular pickup location but no specific search radius distance from that location
			elif not dist1 and latlng1[0] and latlng1[1]  and not dist2 and latlng2[0] and latlng2[1]:
				wanted_ids.append(load.id)
				
				
				
		


		return qs.filter(id__in=wanted_ids)



















class Load(models.Model):

	user        			= models.ForeignKey(User, null=True, blank=True)

	load_id 				= models.CharField(max_length=120, blank=True)
	
	client_name				= models.CharField(max_length=120,blank=True, null=True) # name of company posting the load
	client_phone_number 	= models.CharField(max_length=120, blank=True, null=True)
	payment_terms 			= models.CharField(max_length=120, blank=True, null=True) #e.g. 30 days (End of Month)
	

	pick_up_location 		= models.CharField(max_length=120, blank=True, null=True) # perhaps there is a geo field?
	pick_up_location_lat 	= models.DecimalField(max_digits=10, decimal_places=8, blank=True, null=True)
	pick_up_location_lng 	= models.DecimalField(max_digits=11, decimal_places=8, blank=True, null=True)


	drop_off_location		= models.CharField(max_length=120, blank=True, null=True)
	drop_off_location_lat 	= models.DecimalField(max_digits=10, decimal_places=8, blank=True, null=True)
	drop_off_location_lng 	= models.DecimalField(max_digits=11, decimal_places=8, blank=True, null=True)

	#lat and lng


	pick_up_time 			= models.DateTimeField(auto_now=False, auto_now_add=False, default=datetime.now)
	drop_off_time			= models.DateTimeField(auto_now=False, auto_now_add=False, default=datetime.now)
	#pick up and drop off asap

	vehicle_type			= models.CharField(max_length=120, choices=VEHICLE_CHOICES, blank=True, null=True)

	weight 					= models.DecimalField(max_digits=9, decimal_places=0, null=True, blank=True)
	
	notes					= models.TextField(blank=True, null=True)



	
	#dist_from_user 			= models.CharField(max_length=120, blank=True, null=True)

	#this is the distance from the users current location to the load pickup location
	#dist_from_user 			= models.DecimalField(decimal_places=1, max_digits=20, null=True, blank=True)
	
	#this is the distance from the load dropoff to a user specified location 1
	dist_from_pickup_to_loc1 	= models.DecimalField(decimal_places=1, max_digits=20, null=True, blank=True)

	#this is the distance from the load dropoff to a user specified location 2
	dist_from_dropoff_to_loc2 	= models.DecimalField(decimal_places=1, max_digits=20, null=True, blank=True)

	#this is the dist_from_user + dist_from_loc2_to_dropOff to enable ordering of the loads
	total_distance = models.DecimalField(decimal_places=1, max_digits=20, null=True, blank=True)



	updated     			= models.DateTimeField(auto_now=True, auto_now_add=False)
	timestamp   			= models.DateTimeField(auto_now=False, auto_now_add=True)


	objects = LoadManager()


	def __str__(self):
		return self.load_id


	def get_absolute_url(self):
		return reverse("loads:load_detail_view", kwargs={"load_id": self.load_id})



	# def save(self, *args, **kwargs):
	# 	super(Model, self).save(*args, **kwargs)



	



# def load_pre_save_receiver(sender, instance, *args, **kwargs):
#     if not instance.slug:
#         instance.slug = unique_slug_generator(instance)

# pre_save.connect(load_pre_save_receiver, sender=Load) 



def pre_save_create_load_id(sender, instance, *args, **kwargs):


	# print("User " + request.user)



	if not instance.load_id:
		instance.load_id = unique_load_id_generator(instance)


	#checking to see if this load has been saved before
	qs = Load.objects.filter(pk=instance.pk)
	#print(qs)

	#if the load is not currently in the database then go ahead and geocode the address
	if not qs.exists():
		pick_up_location_lat, pick_up_location_lng = geocode(instance.pick_up_location)
		instance.pick_up_location_lat = pick_up_location_lat
		instance.pick_up_location_lng = pick_up_location_lng

		drop_off_location_lat, drop_off_location_lng = geocode(instance.drop_off_location)
		instance.drop_off_location_lat = drop_off_location_lat
		instance.drop_off_location_lng = drop_off_location_lng

	else:

		#if the load is already in the db but the pickup location has changed then geocode the new location
		if instance.pick_up_location != qs.first().pick_up_location:
			pick_up_location_lat, pick_up_location_lng = geocode(instance.pick_up_location)
			instance.pick_up_location_lat = pick_up_location_lat
			instance.pick_up_location_lng = pick_up_location_lng

		#if the load is already in the db but the dropoff location has changed then geocode the new location
		if instance.drop_off_location != qs.first().drop_off_location:
			drop_off_location_lat, drop_off_location_lng = geocode(instance.drop_off_location)
			instance.drop_off_location_lat = drop_off_location_lat
			instance.drop_off_location_lng = drop_off_location_lng

	


	# just for testing
	if not instance.notes:
		instance.notes = "N/A"

	instance.dist_from_pickup_to_loc1 = 0.0

	# if there is a dist_from_user value then the following is formatting it
	#if instance.dist_from_user:
		#dist = float(str(instance.dist_from_user).replace("km","").replace(" ",""))

		

		# if instance.dist_from_user > 50:
		# 	instance.dist_from_user = str("{:.0f}".format(dist)) #+ " km"
		# else:
		# 	instance.dist_from_user = str("{:.1f}".format(dist)) #+ " km"
			
		#print("New Dist " + instance.dist_from_user)

		




		
	instance.client_name = "DHL"
	instance.client_phone_number = "+353 842169785"

 


pre_save.connect(pre_save_create_load_id, sender=Load)
