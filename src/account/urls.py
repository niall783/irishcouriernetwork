from django.conf.urls import url

from .views import (RegisterView, 
				   LoginView, 
				   AccountHomeView,
				   AccountEmailActivateView,
				   check_your_email,
				   logout_view)

# url(r'^$',views.IndexView.as_view(), name='index'),


app_name = 'account'
urlpatterns = [
		#url(r'^login/$', login_page, name="login"),
		#url(r'^register/$', register_page, name="register"),
		url(r'^login/$', LoginView.as_view(), name="login"),


		url(r'^logout/$', logout_view, name="logout"),
		

		url(r'^register/$', RegisterView.as_view(), name="register"),
		url(r'^home/$', AccountHomeView.as_view(), name="home"),
		url(r'^email/confirm/(?P<key>[0-9A-Za-z]+)/$', AccountEmailActivateView.as_view(), name="email-activate"),
		url(r'^email/resend-activation/$', AccountEmailActivateView.as_view(), name="resend-activation"),
		url(r'^check-your-email/$',check_your_email, name='check-your-email'),

]
