
from django.conf.urls import url
from django.contrib import admin

from .views import (
    UserCreateAPIView,
    UserLoginAPIView
    )


from .views import (PasswordResetView)


urlpatterns = [
    url(r'^login/$', UserLoginAPIView.as_view(), name='login'),
    url(r'^register/$', UserCreateAPIView.as_view(), name='register'),


    # the below endpoint has not been tested
    url(r'^password/reset/$', PasswordResetView.as_view(),name='rest_password_reset'),
]