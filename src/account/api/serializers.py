from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
from django.db.models import Q


from rest_framework.serializers import (
    CharField,
    EmailField,
    HyperlinkedIdentityField,
    ModelSerializer,
    SerializerMethodField,
    ValidationError,
    Serializer
    )

from django.contrib.auth.forms import PasswordResetForm


User = get_user_model()


# class UserDetailSerializer(ModelSerializer):
#     class Meta:
#         model = User
#         fields = [
#             'username',
#             'email',
#             'first_name',
#             'last_name',
#         ]


class UserCreateSerializer(ModelSerializer):
   # I am required to define password2 here because it is not in in the user model
    password2 = CharField(label='Confirm Password')
    class Meta:
        model = User
        fields = [
            'email',
            'password',
            'password2',
            
        ]

        extra_kwargs = {"password":
                            {"write_only": True}
                            }


    def validate_password2(self, value):
        data = self.get_initial()
        password1 = data.get("password")
        password2 = value
        if password1 != password2:
            raise ValidationError("Passwords do not match.")
        return value




    def create(self, validated_data):
        email = validated_data['email']
        password = validated_data['password']
        user_obj = User(
                email = email
            )
        user_obj.set_password(password)
        user_obj.save()
        return validated_data






class UserLoginSerializer(ModelSerializer):
    # token = CharField(allow_blank=True, read_only=True)
    email = EmailField(label='Email Address')
    class Meta:
        model = User
        fields = [
            'email',
            'password',
            # 'token',
            
        ]
        extra_kwargs = {"password":
                            {"write_only": True}
                            }

    def validate(self, data):
        user_obj = None
        email = data.get('email', None)
        password = data['password']
        if not email:
            raise ValidationError("An email is required.")

        user = User.objects.filter(
                Q(email=email)
            ).distinct()

        if user.exists() and user.count() == 1:
            user_obj = user.first()
        else:
            raise ValidationError("This email is not valid.")

        if user_obj:
            if not user_obj.check_password(password):
                raise ValidationError("Incorrect credentials, please try again.")


        # data['token'] = "Some random token"


        return data










# this has not been properly tested as the email client hasn't been hooked up yet
class PasswordResetSerializer(Serializer):
    """
    Serializer for requesting a password reset e-mail.
    """
    email = EmailField()

    password_reset_form_class = PasswordResetForm

    # this is here because it requires a create method to be present
    def create(self, value):
        return {}

    def get_email_options(self):
        """Override this method to change default e-mail options"""
        return {}

    def validate_email(self, value):
        # Create PasswordResetForm with the serializer
        self.reset_form = self.password_reset_form_class(data=self.initial_data)
        if not self.reset_form.is_valid():
            raise serializers.ValidationError(self.reset_form.errors)

        return value